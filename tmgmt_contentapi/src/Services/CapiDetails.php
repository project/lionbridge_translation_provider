<?php

namespace Drupal\tmgmt_contentapi\Services;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\tmgmt_contentapi\Swagger\Client\Api\ProviderApi;
use Drupal\tmgmt_contentapi\Swagger\Client\Api\TokenApi;
use Drupal\tmgmt\TranslatorInterface;

/**
 * Class CapiDetails.
 *
 * Fetch CAPI details at Drupal End.
 *
 * @category Class
 * @package Drupal\tmgmt_contentapi\Services
 * @author Lionbridge Team
 */
class CapiDetails {
  /**
   * Token API object.
   *
   * @var \Drupal\tmgmt_contentapi\Swagger\Client\Api\TokenApi
   */
  protected $capi;

  /**
   * Logger Factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * The database connection.
   *
   * @var \Drupal\tmgmt_contentapi\Swagger\Client\Api\ProviderApi
   */
  protected $provider;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * CapiDetails constructor.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   Logger factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   */
  public function __construct(LoggerChannelFactoryInterface $logger_factory, EntityTypeManagerInterface $entityTypeManager,) {
    $this->loggerFactory = $logger_factory;
    $this->entityTypeManager = $entityTypeManager;

    $this->capi = new TokenApi();
    $this->provider = new ProviderApi();
  }

  /**
   * Function to set the translator.
   */
  protected function getTranslator() {
    // Get all translators.
    $translators = $this->entityTypeManager->getStorage('tmgmt_translator')->loadByProperties(['plugin' => 'contentapi']);
    if (!$translators) {
      return [];
    }
    return $translators;
  }

  /**
   * Get provider details i.e id and name.
   *
   * @return array
   *   Provider array.
   */
  public function getProviderInfo() {
    // Get all translators.
    $translators = $this->getTranslator();
    if (empty($translators)) {
      return [];
    }
    $providersarray = [];
    foreach ($translators as $translator) {
      $capiToken = $this->getCapiToken($translator);
      $providers = $this->provider->providersGet($capiToken);
      if (!empty($providers)) {
        foreach ($providers as $provider) {
          $prid = $provider->getProviderId();
          $prname = $provider->getProviderName();
          $providersarray[$prid] = $prname;
        }
      }
    }
    return $providersarray;
  }

  /**
   * Get capi token.
   *
   * @param \Drupal\tmgmt\TranslatorInterface $translator
   *   Translator object.
   */
  public function getCapiToken(TranslatorInterface $translator) {
    $capisettings = $translator->getSetting('capi-settings');
    return $this->capi->getToken($capisettings['capi_username_ctt'], $capisettings['capi_password_ctt']);
  }

  /**
   * Get capi username of translator.
   *
   * @param \Drupal\tmgmt\TranslatorInterface $translator
   *   Translator object.
   *
   * @return array
   *   Capi username.
   */
  public function getCapiUsernameOfTranslator(TranslatorInterface $translator) {
    $translators = $this->getTranslator();
    if (empty($translators)) {
      return [];
    }
    $capiUsername = [];
    foreach ($translators as $translator) {
      $capiUsername[] = $translator->getSetting('capi-settings')['capi_username_ctt'];
    }
    return $capiUsername;
  }

  /**
   * Function to verify if the capi provider already taken.
   *
   * Remove same from the capi provider array.
   */
  public function popOutAlredyConfiguredProvider($provider_array, $current_provider = '') {
    // Fetch all translators.
    $translators = $this->getTranslator();
    // Get providers set for each translator.
    $configured_providers = [];
    foreach ($translators as $translator) {
      $configured_providers[] = $translator->getSetting('capi-settings')['provider'];
    }
    // Remove already configured providers from the $provider_array.
    foreach ($provider_array as $key => $provider) {
      // Skip the current provider.
      if ($key == $current_provider) {
        continue;
      }
      if (in_array($key, $configured_providers)) {
        unset($provider_array[$key]);
      }
    }
    return $provider_array;
  }

}
