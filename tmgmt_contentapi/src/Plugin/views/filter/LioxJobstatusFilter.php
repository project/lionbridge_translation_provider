<?php

namespace Drupal\tmgmt_contentapi\Plugin\views\filter;

use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\Plugin\views\filter\ManyToOne;
use Drupal\views\ViewExecutable;
use Drupal\views\Views;

/**
 * Filters by phase or status of project.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("liox_job_status_filter")
 */
class LioxJobstatusFilter extends ManyToOne {

  /**
   * The current display.
   *
   * @var string
   *   The current display of the view.
   */
  protected $currentDisplay;

  /**
   * {@inheritdoc}
   */
  #[\ReturnTypeWillChange]
    public function init(ViewExecutable $view, DisplayPluginBase $display, array &$options = NULL) {
    parent::init($view, $display, $options);
    $this->valueTitle = t('Filter by Status');
    $this->definition['options callback'] = [$this, 'generateOptions'];
    $this->currentDisplay = $view->current_display;
    }

    /**
     * Helper function that generates the options.
     *
     * @return array
     *   An array of states and their ids.
     */
    #[\ReturnTypeWillChange]
    public function generateOptions() {

      $options = [
        'SENDING' => t('Sending'),
        'SENT_TO_PROVIDER' => t('Sent to provider'),
        'IN_TRANSLATION' => t('In translation'),
        'REVIEW_TRANSLATION' => t('Review translation'),
        'COMPLETED' => t('Completed'),
        'N/A' => t('N/A'),
      // '6' => t('Unprocessed'),
      ];
      return $options;
      // You can add your custom code here to add custom labels for state transitions.
    }

}
