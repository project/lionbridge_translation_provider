<?php

namespace Drupal\tmgmt_contentapi\Services;

use GuzzleHttp\ClientInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use GuzzleHttp\Psr7\Request;

/**
 * Class to handle throttling and retries.
 */
class HandleThrottling {
  /**
   * Guzzle client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $client;

  /**
   * Logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * HandleThrottling constructor.
   *
   * @var \GuzzleHttp\ClientInterface $client
   */
  public function __construct(ClientInterface $client, LoggerChannelFactoryInterface $logger) {
    $this->client = $client;
    $this->logger = $logger;
  }

  /**
   * Factory method for service instantiation.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The service container.
   *
   * @return static
   *   The instantiated service.
   */
  public static function create(ContainerInterface $container) {
    return new static(
          $container->get('http_client'),
          $container->get('logger.factory')
      );
  }

  /**
   * Handle throttling and retries.
   *
   * @param \GuzzleHttp\Psr7\Request $request
   *   Request.
   * @param array $options
   *   Options.
   * @param int $max_attempts
   *   Maximum attempts.
   * @param int $sleep
   *   Sleep time.
   *
   * @return mixed
   *   Response.
   *
   * @throws \Exception
   *   Exception.
   */
  public function handleThrottling(Request $request, array $options, int $max_attempts = 10, int $sleep = 3) {
    $validResponse = FALSE;
    $attempts = 0;
    $response = NULL;
    while (!$validResponse) {
      try {
        $response = $this->client->send($request, $options);
        $status_code = $response->getStatusCode();
        // Id status code is less than 500 and not 429 or 403, then it is a valid response.
        if (($status_code != 429 && $status_code != 403 && $status_code < 500)) {
          $validResponse = TRUE;
        }
      }
      catch (RequestException $e) {
        $response = $e->getResponse();
        $status_code = $response->getStatusCode();
        if ($response && ($status_code == 502 || $status_code == 403)) {
          $this->logger->get('TMGMT_CONTENTAPI_JOB_POST_ATTEMPT')->info(
                'Re-attempt Method:@method, URI:@uri, status-code:@statuscode, attempts:@attempts',
                [
                  '@method' => $request->getMethod(),
                  '@uri' => (string) $request->getUri(),
                  '@statuscode' => $status_code,
                  '@attempts' => $attempts,
                ]
            );
          $attempts++;
          // Backoff before retrying.
          sleep($sleep);
        }
        else {
          // For other exceptions, rethrow.
          throw $e;
        }
      }
      // Stop retrying if the maximum attempts are exceeded.
      if ($attempts > 9) {
        throw new \Exception("Throttling handler tried 10 times, CAPIV2 unavailable");
      }
    }
    return $response;
  }

}
