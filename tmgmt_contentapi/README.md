# Lionbridge Translation Provider

The **Lionbridge Translation Provider** module integrates Lionbridge translation services with your Drupal site, allowing you to send content for translation directly from Drupal to Lionbridge. This module uses the [Translation Management Tool (TMGMT)](https://www.drupal.org/project/tmgmt) to manage translation jobs and integrates Lionbridge as a provider within the TMGMT framework.

## Requirements

- **Drupal**: This module is compatible with Drupal 9 and 10.
- **TMGMT (Translation Management Tool)**: Required to manage and submit translation jobs.
- **Lionbridge API Credentials**: Needed to authenticate and interact with the Lionbridge service.

## Installation

1. **Install the module**:
   - Download and install the module through Composer:
     ```bash
     composer require drupal/lionbridge_translation_provider
     ```
   - Enable the module:
     ```bash
     drush en lionbridge_translation_provider -y
     ```

2. **Configure API credentials**:
   - Go to **Configuration** > **Translation Management** > **Lionbridge Provider Settings**.
   - Enter the API credentials (API key, secret, etc.) provided by Lionbridge to authenticate the module.

## Configuration

1. **Set up Lionbridge as a Translation Provider**:
   - Go to **Configuration** > **Translation Management** > **Providers**.
   - Select **Lionbridge** from the list of available providers and configure it with your Lionbridge credentials.
   - Adjust additional settings as needed, such as supported languages, callback URLs, and error handling options.

2. **Job Settings**:
   - Define job-specific settings under **Lionbridge Provider Settings**.
   - Customize settings related to job batching, file formats, and translation memory.

## Usage

1. **Creating a Translation Job**:
   - Navigate to **Translation** > **Source**.
   - Add the content items you wish to translate and select the languages required.
   - To send items to more than one language, add the item to the cart.
   - After adding items, go to the cart, select the nodes and languages, and click on **Request Translation**.
   - Select **Lionbridge Content API Connector** as the provider and submit the jobs one by one or all at once.
   - Job submission to the provider is processed in the background via a queue, so it may take some time for the job to reach active status on the Job Overview page.

2. **Submitting and Tracking Translations**:
   - After creating the job, submit it to Lionbridge for translation.
   - Use the **Translation** > **Jobs** (Job Overview) page to track the status and progress of each translation job.
   - Once the translation is complete, the translated content will be available in Drupal.

3. **Import Translation Job**
   - Import will work as per the set configuration under Lionbridge providers.
   - Auto import will aknowledge the updates from all lionbridge provider and add it to storage.
   - Import will happen in background via queue.

3. **Error Handling**:
   - In case of errors, refer to **TMGMT_CONTENTAPI** under **Reports**.
   - Check for details on any failed jobs, and retry or address any issues as necessary.

## Troubleshooting

- **Common Issues**:
   - **Authentication Errors**: Double-check your API credentials in the **Lionbridge Provider Settings**.
   - **API Throttling**: If submitting a large number of jobs, consider configuring rate-limiting options to avoid reaching API limits.
   - **FastCGI or Timeout Issues**: Ensure your server configuration can handle the payloads being sent. Check settings like `fastcgi_buffer_size` if on Pantheon or similar environments.

- **Support**:
   - Visit the [module’s Drupal project page](https://www.drupal.org/project/lionbridge_translation_provider) for further documentation and community support.

## Maintainers

This module is actively maintained by the Drupal community. Contributions and issues can be reported on the [module's issue queue on Drupal.org](https://www.drupal.org/project/issues/lionbridge_translation_provider).

## License

This project is licensed under the [GNU General Public License, version 2](https://www.gnu.org/licenses/old-licenses/gpl-2.0.html), or later.
