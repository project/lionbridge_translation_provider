<?php

namespace Drupal\tmgmt_contentapi\Services;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Render\Markup;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;
use Drupal\tmgmt\JobInterface;
use Drupal\tmgmt\JobQueue;
use Drupal\tmgmt\Entity\Job;
use Drupal\tmgmt_contentapi\Services\CapiDataProcessor;
use Drupal\tmgmt_contentapi\Swagger\Client\Api\JobApi;
use Drupal\tmgmt_contentapi\Swagger\Client\Api\RequestApi;
use Drupal\tmgmt_contentapi\Swagger\Client\Api\SourceFileApi;
use Drupal\tmgmt_contentapi\Swagger\Client\Model\CreateJob;
use Drupal\tmgmt_contentapi\Swagger\Client\Model\CreateRequestFile;
use Drupal\tmgmt_contentapi\Swagger\Client\Model\ProviderId;
use Drupal\tmgmt_contentapi\Services\JobHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\tmgmt_contentapi\Swagger\Client\Api\TranslationMemoryApi;
use Drupal\tmgmt_contentapi\Swagger\Client\Model\CreateRequestUpdateTM;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\tmgmt_contentapi\Services\QueueOperations;

/**
 * Service to create a job.
 */
class CreateConnectorJob {
  use StringTranslationTrait;

  /**
   * The array for content api bundle.
   *
   * @var array
   */
  protected $contentApiBundle;

  /**
   * The contains job request object.
   *
   * @var \Drupal\tmgmt_contentapi\Swagger\Client\Model\CreateJob
   */
  protected $jobRequest;

  /**
   * The contains job api object.
   *
   * @var \Drupal\tmgmt_contentapi\Swagger\Client\Api\JobApi
   */
  protected $jobApi;

  /**
   * The contains cp Jobs.
   *
   * @var \Drupal\tmgmt_contentapi\Swagger\Client\Model\Job
   */
  protected $createdCpJob;

  /**
   * The Source file api.
   *
   * @var \Drupal\tmgmt_contentapi\Swagger\Client\Api\SourceFileApi
   */
  protected $fileApi;

  /**
   * The request api.
   *
   * @var \Drupal\tmgmt_contentapi\Swagger\Client\Api\RequestApi
   */
  protected $transRequestApi;

  /**
   * The transfer file service.
   *
   * @var \Drupal\tmgmt_contentapi\Services\ExportJobFiles
   */
  protected $transferFiles;

  /**
   * Token to submit the job.
   *
   * @var string
   */
  protected $capiToken;

  /**
   * Token API object.
   *
   * @var array
   */
  protected $capi;

  /**
   * The stream wrapper service.
   *
   * @var \Drupal\Core\StreamWrapper\StreamWrapperManager
   */
  protected $streamWrapperManager;

  /**
   * The filesystem service.
   *
   * @var \Drupal\Core\Datetime\DrupalDateTime
   */
  protected $drupalDateTime;

  /**
   * Message to pass when job created.
   *
   * @var string
   */
  protected $messageToPass;

  /**
   * The capi data processor service.
   *
   * @var \Drupal\tmgmt_contentapi\Services\CapiDataProcessor
   */
  protected $capiDataProcessor;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Files array to delete if exception occures.
   *
   * @var array
   */
  protected $fileArrayToDelte;

  /**
   * Zip path of current job.
   *
   * @var string
   */
  protected $zipPath;

  /**
   * Zip Archive path.
   *
   * @var obj
   */
  protected $zipArchivePath;

  /**
   * The queue factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * The queue object used for processing jobs.
   *
   * @var object
   */
  protected $queue;

  /**
   * The job queue service.
   *
   * @var \Drupal\tmgmt\JobQueue
   */
  protected $jobQueue;

  /**
   * The queue object used for processing job items.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $itemQueue;

  /**
   * Logger Factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * Job helper service.
   *
   * @var \Drupal\tmgmt_contentapi\Services\JobHelper
   */
  protected $jobHelper;

  /**
   * The queue operations.
   *
   * @var \Drupal\tmgmt_contentapi\Services\QueueOperations
   */
  protected $queueOperations;


  /**
   * State key for queue worker.
   */
  const QUEUE_WORKER_EXPORT_TRANSLATION_JOBS_TO_CAPI = 'queue_worker_export_translation_jobs_to_capi';

  /**
   * Queue name for export translation jobs to capi.
   */
  const QUEUE_NAME_EXPORT_JOBS = 'export_translation_jobs_to_capi';

  /**
   * Consructor.
   *
   * @param ExportJobFiles $transferFiles
   *   Object for custom service export job files.
   * @param \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface $streamWrapperManager
   *   Object for stream wrapper manager service.
   * @param \Drupal\tmgmt_contentapi\Services\CapiDataProcessor $capiDataProcessor
   *   Object for capi data processor service.
   * @param \Drupal\tmgmt\JobQueue $jobQueue
   *   Job queue object.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   Service for logger factory.
   * @param \Drupal\tmgmt_contentapi\Services\JobHelper $jobHelper
   *   Service for job helper.
   * @param \Drupal\Core\Queue\QueueFactory $queueFactory
   *   Service for queue factory.
   * @param \Drupal\tmgmt_contentapi\Services\QueueOperations $queueOperations
   *   Service for queue operations.
   */
  public function __construct(
        ExportJobFiles $transferFiles,
        StreamWrapperManagerInterface $streamWrapperManager,
        CapiDataProcessor $capiDataProcessor,
        JobQueue $jobQueue,
        LoggerChannelFactoryInterface $logger,
        JobHelper $jobHelper,
        QueueFactory $queueFactory,
        QueueOperations $queueOperations,
    ) {

    $this->transferFiles = $transferFiles;
    $this->streamWrapperManager = $streamWrapperManager;
    $this->capiDataProcessor = $capiDataProcessor;
    $this->jobQueue = $jobQueue;
    $this->logger = $logger;
    $this->jobHelper = $jobHelper;
    $this->queueFactory = $queueFactory;
    $this->queueOperations = $queueOperations;
    // Set default values for variables.
    $this->itemQueue = $this->queueFactory->get(self::QUEUE_NAME_EXPORT_JOBS);
    $this->contentApiBundle = [];
    $this->jobRequest = NULL;
    $this->jobApi = new JobApi();
    $this->messageToPass = $this->t('Exported files can be downloaded here: <br/>');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
          $container->get('tmgmt_contentapi.export_job'),
          $container->get('stream_wrapper_manager'),
          $container->get('tmgmt_contentapi.capi_data_processor'),
          $container->get('tmgmt.queue'),
          $container->get('logger.factory'),
          $container->get('tmgmt_contentapi.job_helper'),
          $container->get('state'),
          $container->get('queue'),
      );
  }

  /**
   * Function to set jobRequest.
   *
   * @param \Drupal\tmgmt_contentapi\Swagger\Client\Model\CreateJob $value
   *   Set jobRequest object.
   */
  public function setJobRequest(CreateJob $value) {
    return $this->jobRequest = $value;
  }

  /**
   * Function to set jobApi.
   *
   * @param \Drupal\tmgmt_contentapi\Swagger\Client\Api\JobApi $value
   *   Set job api.
   */
  public function setJobApi(JobApi $value) {
    return $this->jobApi = $value;
  }

  /**
   * Function to set createdCpJob.
   *
   * @param \Drupal\tmgmt_contentapi\Swagger\Client\Model\Job $value
   *   Set job api.
   */
  public function setCreatedCpJob(Job $value) {
    return $this->createdCpJob = $value;
  }

  /**
   * Function to set drupal date time.
   *
   * @param \Drupal\Core\Datetime\DrupalDateTime $value
   *   Set drupal date and time object.
   */
  public function setDrupalDateTime(DrupalDateTime $value) {
    return $this->drupalDateTime = $value;
  }

  /**
   * Function to get job files.
   *
   * @param \Drupal\tmgmt\JobInterface $job
   *   Job under process.
   * @param bool $is_translation_memory
   *   Check if job is for translation memory.
   *
   * @return array
   *   job file to transfer
   */
  public function getJobFilesToTransfer(JobInterface $job, bool $is_translation_memory = FALSE) {
    return $this->transferFiles->jobFilesToTransfer($job, $is_translation_memory);
  }

  /**
   * Function to get reference files.
   *
   * @param \Drupal\tmgmt\JobInterface $job
   *   Job under process.
   *
   * @return array
   *   reference files to transfer
   */
  public function getRefFilesToTransfer(JobInterface $job) {
    return $this->transferFiles->refFilesToTransfer($job);
  }

  /**
   * Summary of genrateJobRequst.
   *
   * @param \Drupal\tmgmt\JobInterface $job
   *   Job under process.
   *
   * @return \Drupal\tmgmt_contentapi\Swagger\Client\Model\CreateJob
   *   Request job object.
   */
  public function genrateJobRequst(JobInterface $job) {
    $source_lang = $job->getRemoteSourceLanguage();
    $target_lang = $job->getRemoteTargetLanguage();
    $shouldquote = (bool) ($job->getSetting('capi-settings')['quote']['is_quote']);
    $capijobsettings = $job->getSetting("capi-settings");
    $jobarray = [
      'job_name' => $this->jobHelper->getJobLabelNoSpeChars($job),
      'description' => $capijobsettings["description"] ?? NULL,
      'po_reference' => $capijobsettings["po_reference"] ?? NULL,
      'due_date' => $capijobsettings["due_date"] ?? NULL,
      'should_quote' => $shouldquote,
      'source_lang_code' => $source_lang,
      'target_lang_code' => $target_lang,
      'connector_name' => 'Lionbridge Connector for Drupal 9 and 10',
      'connector_version' => 'V 9.3.2',
      ];
    if (isset($capijobsettings["custom_data"]) && $capijobsettings["custom_data"] !== "") {
      $job['custom_data'] = $capijobsettings["custom_data"];
    }
    $jobrequest = new CreateJob($jobarray);
    return $jobrequest;
  }

  /**
   * Function to create cp jobs.
   *
   * @param \Drupal\tmgmt\JobInterface $job
   *   Job under process.
   */
  public function createCpJobs(JobInterface $job) {
    // Create cp job.
    $this->setJobRequest($this->genrateJobRequst($job));
    $translator = $job->getTranslator();
    $this->capiToken = \DRUPAL::service('tmgmt_contentapi.capi_details')->getCapiToken($translator);
    $this->createdCpJob = $this->jobApi->jobsPost($this->capiToken, $this->jobRequest);
  }

  /**
   * Function to transfer files to cpa.
   *
   * @param \Drupal\tmgmt\JobInterface $job
   *   Job under process.
   */
  public function uploadTransferFilesToCpa(JobInterface $job) {

    // Save job to make sure all settings are available later when job queue and item queue are processed.
    $job->save();
    $upload_info = [
      'item_id' => $job->id(),
      'job' => $job,
    ];
    $this->itemQueue->createItem($upload_info);
    $remainingJobCount = $this->jobQueue->count();
    // Check if lock is set for queue worker. No need to start new queue worker if already started.
    if ($this->queueOperations->getQueueWorkerLockStatus(self::QUEUE_WORKER_EXPORT_TRANSLATION_JOBS_TO_CAPI)) {
      return;
    }
    $workers = 1;
    // If this is last jobs in process then start queue item processing.
    // Execut queue using only one worker. Multiple workers leads to item stuck in queue.
    // If job item alread exist in queue it means queue worker is already started.
    // Then no need to create new queue worker.
    if ($remainingJobCount <= $workers) {
      $this->queueOperations->setQueueWorkerLock(self::QUEUE_WORKER_EXPORT_TRANSLATION_JOBS_TO_CAPI);
      $this->queueOperations->processQueue('export_translation_jobs_to_capi');
    }
  }

  /**
   * Function to batch submit files to CAPI.
   *
   * @param \Drupal\tmgmt\JobInterface $job
   *   Job under process.
   * @param bool $is_translation_memory
   *   Check if job is for translation memory.
   */
  public function batchSubmittoUploadFilesToCapi(JobInterface $job, $is_translation_memory = FALSE) {
    // Check if gettranslator is not null.
    try {
      $this->contentApiBundle = [];
      $transfer_files = [];
      $this->createdCpJob = NULL;
      $transfer_files = $this->getJobFilesToTransfer($job, $is_translation_memory);
      if (!$is_translation_memory) {
        $this->createCpJobs($job);
        $capi_job_id = $this->createdCpJob->getJobId();
      }
      else {
        $capi_job_id = $this->jobHelper->getCpJobIdfromLocJob($job);
      }
      // Upload files from transferfiles array to CPA.

      $fileApi = new SourceFileApi();
      $transRequestApi = new RequestApi();

      // Array to store temporally translation requst and associated files.

      $this->logger->get("TMGMT_CONTENTAPI_JOB_POST")->notice(
            'Job sending started for job ids: @jobids ',
            [
              '@jobids' => $job->id() . '_' . $capi_job_id,
            ]
        );
      foreach ($transfer_files as $id => $tmpfile) {
        $data = [];
        $data["filename"] = $tmpfile->getFilename();
        $stmrg = $this->streamWrapperManager->getViaUri($tmpfile->getFileUri());
        $extpath = $stmrg->realpath();
        $zip = new \ZipArchive();
        if ($zip->open($extpath) === TRUE) {
          $zip->extractTo($this->transferFiles->allFilesPath);
          $zip->close();
        }
        $data["filetype"] = $tmpfile->getMimeType();
        $stmrg = $this->streamWrapperManager->getViaUri($tmpfile->getFileUri());
        $extpath = $stmrg->realpath();
        try {
          $filerequest = new \SplFileObject($extpath);
          // File upload to TUS CLINET.
          $name = $data["filename"];
          $jobpath = $job->getTranslator()->getSetting('transfer-settings') ? $extpath : $this->transferFiles->allFilesPath . "/" . $name;
          $contentapitmpfile = $fileApi->jobsJobIdUploadPost($this->capiToken, $capi_job_id, $data["filename"], $data["filetype"], $filerequest);
          $fileApi->uploadFile($contentapitmpfile, $jobpath);
          // If request is for translation memory then update translation memory.
          // Else continue with file upload for job creation.
          if ($is_translation_memory) {
            $tmupdateapi = new TranslationMemoryApi();
            $tmupdaterequest = new CreateRequestUpdateTM(
              [
              'file_id' => $contentapitmpfile->getfms_file_id(),
              'source_native_language_code' => $job->getRemoteSourceLanguage(),
              'target_native_language_code' => $job->getRemoteTargetLanguage(),
              ]
              );
            $tmupdateapi->jobsJobIdTmUpdatefilePut($this->capiToken, $capi_job_id, $tmupdaterequest);
          }
          else {
            $request = new CreateRequestFile([
              "request_name" => $job->id() . "_" . $id . "_" . $job->getRemoteSourceLanguage() . "-" . $job->getRemoteTargetLanguage(),
              "source_native_id" => $job->id() . "_" . $id,
              "source_native_language_code" => $job->getRemoteSourceLanguage(),
              "target_native_language_codes" => [$job->getRemoteTargetLanguage()],
              "file_id" => $contentapitmpfile['fms_file_id'],
            ]);
            $contentapitmprequst = $transRequestApi->jobsJobIdRequestsAddfilePost($this->capiToken, $capi_job_id, $request);
            $this->contentApiBundle[] = $contentapitmprequst;
          }
        }
        catch (\Exception $ex) {
          $this->logger->get("TMGMT_CONTENTAPI_FILE_POST_FAILED")->notice(
            'jobids: @jobids - Failed File posting for file:@filename, Error: @errormessage ',
            [
              '@jobids' => $job->id() . '_' . $capi_job_id,
              '@filename' => $data["filename"],
              '@errormessage' => $ex->getMessage(),
            ]
                );
        }
        unset($contentapitmpfile);
        unset($contentapitmprequst);
        unset($request);
      }
      if (!$is_translation_memory) {
        $this->submitJobPostUpload($job, $capi_job_id);
      }
    }
    catch (\Exception $ex) {
      if (!$is_translation_memory) {
        // Release job froom queue if any exception occurs.
        $this->releaseJobFromQueue($job);
        if (isset($this->createdCpJob)) {
          $capi_job_id = $this->createdCpJob->getJobId();
        }
      }
      else {
        $capi_job_id = $this->jobHelper->getCpJobIdfromLocJob($job);
      }
      // Log the error.
      $this->logger->get(channel: 'TMGMT_CONTENTAPI_JOB_POST')->error(
            'Issue while processing item : @joblabel, Job: @job, CAPIjobid: @capijobid Error: @errormessage.',
            context: [
              '@joblabel' => $job->label(),
              '@job' => $job->id(),
              '@capijobid' => $capi_job_id,
              '@errormessage' => $ex->getMessage(),
            ]
            );
      $this->cleanUpJobOnFailure($job, $ex);
      // Check if this is last job in queue then release lock.
      $this->queueOperations->releaseQueueWorkerLock(self::QUEUE_WORKER_EXPORT_TRANSLATION_JOBS_TO_CAPI, self::QUEUE_NAME_EXPORT_JOBS);
    }
    // Free memory to avoid memory leak.
    unset($filerequest);
    unset($request);
    unset($job);
    unset($contentapitmprequst);
    unset($contentapitmpfile);
    unset($this->contentApiBundle);
    unset($transfer_files);
    unset($this->createdCpJob);
    unset($data);
    // Collect garbage.
    gc_collect_cycles();
  }

  /**
   * Function to submit job to content API.
   *
   * @param \Drupal\tmgmt\JobInterface $job
   *   Job under process.
   */
  public function submitJobToContentApi(JobInterface $job) {
    try {
      $this->setDrupalDateTime(new DrupalDateTime('now', 'UTC'));
      $this->uploadTransferFilesToCpa($job);
      $this->getRefFilesToTransfer($job);
      // Generate message which user can see once job added to queue for further processing.
      $messageToPass = $this->t('Job(s) creation is in progress and is running in the background. You can view the progress and updated status on the Job Overview page.');
      \Drupal::messenger()->addMessage(Markup::create($messageToPass));
    }
    catch (\Exception $ex) {
      $this->releaseJobFromQueue($job);
      $this->logger->get('TMGMT_CONTENTAPI_JOB_POST')->error(
            'Issue while creating job : @joblabel, Error: @errormessage.',
            [
              '@joblabel' => $job->label(),
              '@errormessage' => $ex->getMessage(),
            ]
            );
      $this->cleanUpJobOnFailure($job, $ex);
      // Check if this is last job in queue then release lock.
      $this->queueOperations->releaseQueueWorkerLock(self::QUEUE_WORKER_EXPORT_TRANSLATION_JOBS_TO_CAPI, self::QUEUE_NAME_EXPORT_JOBS);
    }
  }

  /**
   * Function to submit translation memory to content API.
   *
   * @param \Drupal\tmgmt\JobInterface $job
   *   Job under process.
   */
  public function updateTranslationMemory(JobInterface $job) {
    // If job state is not finished then return.
    if ($job->getState() != JobInterface::STATE_FINISHED) {
      return;
    }
    // Submit translation memory to content api.
    $translator = $job->getTranslator();
    $this->capiToken = \DRUPAL::service('tmgmt_contentapi.capi_details')->getCapiToken($translator);
    // If job submission for translation memory.
    $this->batchSubmittoUploadFilesToCapi($job, TRUE);
    $this->transferFiles->deleteProcessedFiles();
    $job->submitted($this->t("Job sent to update translation memory!"));
    $messageToPass = $this->t('TM update request successfully submitted.<br/>Exported files can be downoaded here:<br/>');
    $zipDownload = '';
    if (isset($this->transferFiles->zipPath)) {
      $zipDownload = \Drupal::service('file_url_generator')->generateAbsoluteString($this->transferFiles->zipPath);
    }
    $messageTopass .= '<a href="' . $zipDownload . '">' . Xss::filter($this->jobHelper->getJobLabelNoSpeChars($job)) . '</a>';
    \Drupal::messenger()->addMessage(Markup::create($messageToPass));
  }

  /**
   * Function to submit job to content API.
   *
   * @param \Drupal\tmgmt\JobInterface $job
   *   Job under process.
   * @param string $capi_job_id
   *   Capi job id.
   */
  public function submitJobPostUpload(JobInterface $job, string $capi_job_id) {
    try {
      $pr = $job->getSetting("capi-settings")["provider"];
      $jid = $capi_job_id;
      $prmodel = new ProviderId(['provider_id' => $pr]);
      $jobsubmitresult = $this->jobApi->jobsJobIdSubmitPut($this->capiToken, $jid, $prmodel);
      if ($jobsubmitresult) {
        // Add content api bundle to job's settings.
        $this->jobHelper->addCpaSettingsToJob($job, serialize($this->contentApiBundle));
        // Generate message which user can see once job created.
        $this->messageToPass .= '<a href="' . \Drupal::service('file_url_generator')->generateAbsoluteString($this->transferFiles->zipPath) . '">' . Xss::filter($this->jobHelper->getJobLabelNoSpeChars($job)) . '</a>';
        \Drupal::messenger()->addMessage(Markup::create($this->messageToPass));
        // Set message if quote is set else success.
        if ($job->getSetting('capi-settings')['quote']['is_quote']) {
          $job->submitted($this->t("This job was submitted for a quote. To submit your job for processing, you must log into your translation provider's system to approve this quote."));
        }
        else {
          $job->submitted($this->t("Job sent to provider!"));
        }

        // Store job details job status table for further processing.
        $this->capiDataProcessor->jobToinsertInProcessor($job, $jobsubmitresult->getProviderId());
        $this->transferFiles->deleteProcessedFiles();
        $this->logger->get("TMGMT_CONTENTAPI_JOB_POST")->notice('Job sent successfully job ids: @jobids ', ['@jobids' => $job->id() . '_' . $jid]);
        // Check if this is last job in queue then release lock.
        $this->queueOperations->releaseQueueWorkerLock(self::QUEUE_WORKER_EXPORT_TRANSLATION_JOBS_TO_CAPI, self::QUEUE_NAME_EXPORT_JOBS);
      }
    }
    catch (\Exception $ex) {
      // Release job from queue if any exception occurs.
      $this->releaseJobFromQueue($job);
      // Log the reson for error.
      $this->logger->get('TMGMT_CONTENTAPI_JOB_POST')->error(
            'Failed to process job, please resubmit the same job: @joblabel, jobid: @jobids, language: @languages Error: @errormessage.',
            context: [
              '@joblabel' => $job->label(),
              '@jobids' => $job->id(),
              '@languages' => $job->getRemoteSourceLanguage() . ' to ' . $job->getRemoteTargetLanguage(),
              '@errormessage' => $ex->getMessage(),
            ]
            );
      $this->cleanUpJobOnFailure($job, $ex);
      // Check if this is last job in queue then release lock.
      $this->queueOperations->releaseQueueWorkerLock(self::QUEUE_WORKER_EXPORT_TRANSLATION_JOBS_TO_CAPI, self::QUEUE_NAME_EXPORT_JOBS);
    }
  }

  /**
   * If exception occurs, clean up everything: delete exported files, cancel job in CA if any.
   */
  public function cleanUpJobOnFailure(JobInterface $job, \Exception $ex) {
    // If exception occurs, clean up everything: delete exported files, cancel job in CA if any.
    foreach ($this->transferFiles->fileArrayToDelte as $tempfile) {
      if (file_exists($tempfile->getFileUri())) {
        \Drupal::service('file_system')->delete($tempfile->getFileUri());
      }
    }
    $job->setState(JobInterface::STATE_UNPROCESSED);
    try {
      if (isset($this->createdCpJob)) {
        $this->jobApi->jobsJobIdDelete($this->capiToken, $this->createdCpJob->getJobId());
      }
    }
    catch (\Exception $exception) {
      $msg = $this->t('Tried to delete Job in Content API, but failed: @message', ['@message' => $exception->getMessage()]);
      if (strlen($msg) > 200) {
        $msg = substr($msg, 0, 200);
      }
      \Drupal::messenger()->addMessage($msg, "error");
    }
    // Deleting zip.
    $zipfileobj = $this->jobHelper->createFileObject($this->transferFiles->zipPath);
    if ($zipfileobj->getFileUri() != NULL) {
      \Drupal::service('file_system')->delete($zipfileobj->getFileUri());
    }
    if ($this->transferFiles->allFilesPath != NULL) {
      \Drupal::service('file_system')->deleteRecursive($this->transferFiles->allFilesPath);
    }
    // Set exact error occured while job creation.
    $msg = $ex->getMessage();
    if (strlen($msg) > 200) {
      $msg = substr($msg, 0, 200);
    }
    \Drupal::messenger()->addMessage($job->label() . $msg, 'error');
  }

  /**
   * Release job from queue if any exception occurs.
   *
   * @param \Drupal\tmgmt\JobInterface $job
   *   Job under process.
   */
  protected function releaseJobFromQueue(JobInterface $job) {
    $upload_info = [
      'job' => $job,
    ];
    $this->itemQueue->releaseItem($upload_info);
  }

}
