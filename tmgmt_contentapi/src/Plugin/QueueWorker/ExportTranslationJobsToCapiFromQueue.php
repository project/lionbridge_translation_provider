<?php

namespace Drupal\tmgmt_contentapi\Plugin\QueueWorker;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Process a queue of translated jobs.
 *
 * @QueueWorker(
 *   id = "export_translation_jobs_to_capi",
 *   title = @Translation("Send jobs for translation to CAPI"),
 *   items = {100}
 * )
 */
class ExportTranslationJobsToCapiFromQueue extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * Constructs a new class instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $job = $data['job'];
    \Drupal::service('tmgmt_contentapi.create_job')->batchSubmittoUploadFilesToCapi($job);
  }

}
