<?php

namespace Drupal\tmgmt_contentapi\Plugin\views\filter;

use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\Plugin\views\filter\ManyToOne;
use Drupal\views\ViewExecutable;
use Drupal\views\Views;

/**
 * Filters by phase or status of project.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("string")
 */
class LioxJobIdFilter extends ManyToOne {

  /**
   * The current display.
   *
   * @var string
   *   The current display of the view.
   */
  protected $currentDisplay;

  /**
   * {@inheritdoc}
   */
  #[\ReturnTypeWillChange]
  public function init(ViewExecutable $view, DisplayPluginBase $display, array &$options = NULL) {
    parent::init($view, $display, $options);
    $this->valueTitle = t('Filter by Status');
  }

  /**
   * Helper function that generates the options.
   *
   * @return array
   *   An array of states and their ids.
   */
  #[\ReturnTypeWillChange]
    public function generateOptions() {
    // You can add your custom code here to add custom labels for state transitions.
    return [];
    }

    /**
     * Helper function that builds the query.
     */
    #[\ReturnTypeWillChange]
    public function query() {
      echo "<pre>";
      print_r($this->value);
      exit;
    }

}
