<?php

namespace Drupal\tmgmt_contentapi\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Queue\QueueInterface;
use Drupal\Core\Queue\QueueFactory;

/**
 * Provides a 'Queue Status' Block.
 *
 * @Block(
 *   id = "queue_status_block",
 *   admin_label = @Translation("Queue Status Block"),
 * )
 */
class QueueStatusBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Get the queue information.
    $queue_factory = \Drupal::service('queue');
    $queues = ['export_translation_jobs_to_capi'];

    $status = [];
    foreach ($queues as $queue_name) {
      /** @var \Drupal\Core\Queue\QueueInterface $queue */
      $queue = $queue_factory->get($queue_name);
      $queue_items = $queue->numberOfItems();
      $status[] = [
        'name' => $queue_name,
        'items' => $queue_items,
      ];
    }

    // Render the queue statuses in a table.
    return [
      '#type' => 'table',
      '#header' => [
        $this->t('Queue Name'),
        $this->t('Items in Queue'),
      ],
      '#rows' => array_map(fn($item) => [$item['name'], $item['items']], $status),
      '#attributes' => [
        'class' => ['queue-status-table'],
      ],
      '#attached' => [
        'library' => [
          'tmgmt_contentapi/queue-status-block',
        ],
      ],
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }

}
