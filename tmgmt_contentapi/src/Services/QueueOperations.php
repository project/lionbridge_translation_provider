<?php

namespace Drupal\tmgmt_contentapi\Services;

use Drupal\Core\Database\Connection;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\State\StateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Service for handling queue operations.
 */
class QueueOperations {
  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The queue factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * The state service for storing temporary data.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Constructs a QueueOperations service.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   The logger service.
   * @param \Drupal\Core\Queue\QueueFactory $queueFactory
   *   The queue object used for processing job items.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   */
  public function __construct(Connection $database, LoggerChannelFactoryInterface $logger, QueueFactory $queueFactory, StateInterface $state) {
    $this->database = $database;
    $this->logger = $logger;
    $this->queueFactory = $queueFactory;
    $this->state = $state;
  }

  /**
   * Factory method for service instantiation.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The service container.
   *
   * @return static
   *   The instantiated service.
   */
  public static function create(ContainerInterface $container) {
    return new static(
          $container->get('database'),
          $container->get('logger.factory'),
          $container->get('queue'),
          $container->get('state')
      );
  }

  /**
   * Logic to start workers for processing the queue.
   *
   * @param string $queue_name
   *   The name of the queue.
   */
  public function processQueue(string $queue_name) {
    // Check if Drush is available.
    $drush_path = trim(shell_exec('which drush'));
    if (empty($drush_path)) {
      $this->logger->get('TMGMT_CONTENTAPI')->error('Drush command not found. Queue processing aborted.');
      return;
    }

    // Build the Drush command to run the queue.
    $cmd = escapeshellcmd("{$drush_path} queue:run {$queue_name}") . " > /dev/null 2>&1 &";

    // Execute the command.
    shell_exec($cmd);
  }

  /**
   * Function to release queue worker lock.
   *
   * @param string $state_key
   *   The state key to store the lock status.
   * @param string $queue_name
   *   The queue object.
   */
  public function releaseQueueWorkerLock(string $state_key, string $queue_name) {
    // Release lock if last item has just processed.
    // So at next job creation new queue worker can start processing.
    $number_of_items = $this->queueFactory->get($queue_name)->numberOfItems();
    if ($number_of_items == 1) {
      $this->state->set($state_key, ['status' => 'unlocked', 'timestamp' => time()]);
      $this->logger->get('TMGMT_CONTENTAPI_LOCK')->notice(
            'Releasing lock for queue worker Key: @state_key, Queue: @queue_name',
            [
              '@state_key' => $state_key,
              '@queue_name' => $queue_name,
            ]
        );
    }
  }

  /**
   * Function to set queue worker lock with timestamp.
   *
   * @param string $state_key
   *   The state key to store the lock status.
   */
  public function setQueueWorkerLock($state_key) {
    // Release lock if last item has just processed.
    // So at next job creation new queue worker can start processing.
    $this->state->set($state_key, ['status' => 'locked', 'timestamp' => time()]);
    $this->logger->get('TMGMT_CONTENTAPI_LOCK')->notice('Setting lock for queue worker: @state_key', ['@state_key' => $state_key]);
  }

  /**
   * Function to get queue worker lock status.
   *
   * @param string $state_key
   *   The state key to store the lock status.
   *
   * @return bool
   *   TRUE if lock is set, FALSE otherwise.
   */
  public function getQueueWorkerLockStatus(string $state_key) {
    $lock = $this->state->get($state_key,
    [
      'status' => 'unlocked',
      'timestamp' => 0
    ]);
    // If lock status is not found, then return TRUE.
    if (!isset($lock['status'])) {
      return FALSE;
    }
    if ($lock['status'] == 'locked') {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Function to remove queue worker lock.
   *
   * This is to handle the case when queue worker is stuck and not releasing the lock.
   *
   * In such case, we can remove the lock after provided timeout hours, default is 3 hours.
   *
   * @param string $state_key
   *   The state key to store the lock status.
   * @param int $timeout
   *   The timeout in seconds.
   */
  public function removeQueueWorkerLock($state_key, $timeout = 10800) {
    $lock = $this->state->get($state_key, ['status' => 'locked']);
    if (!isset($lock['status'])) {
      return;
    }
    // If lock is older than 3 hours, then set it to unlocked.
    if ($lock['status'] === 'locked' && (time() - $lock['timestamp']) > $timeout) {
      $this->state->set($state_key,
      [
        'status' => 'unlocked',
        'timestamp' => time(),
      ]);
      // Log the event.
      $this->logger->get('TMGMT_CONTENTAPI_LOCK')->notice('Removing stale lock for queue worker: @state_key, @timeout',
      [
        '@state_key' => $state_key,
        '@timeout' => $timeout,
      ]);
    }
  }

}
