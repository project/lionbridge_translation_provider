<?php

namespace Drupal\tmgmt_contentapi\Plugin\tmgmt\Translator;

use Drupal\tmgmt\JobInterface;
use Drupal\tmgmt\Translator\TranslatableResult;
use Drupal\tmgmt\TranslatorInterface;
use Drupal\tmgmt\TranslatorPluginBase;


/**
 * Content API Translator.
 *
 * @TranslatorPlugin(
 *   id = "contentapi",
 *   label = @Translation("Lionbridge Content API Connector"),
 *   logo = "icons/lionbridge.png",
 *   description = @Translation("Provider to send content to Lionbridge"),
 *   ui = "Drupal\tmgmt_contentapi\ContentApiTranslatorUI"
 * )
 */
class ContentApiTranslator extends TranslatorPluginBase {

  /**
   * {@inheritdoc}
   */
  #[\ReturnTypeWillChange]
    public function checkTranslatable(TranslatorInterface $translator, JobInterface $job) {
    // Anything can be exported.
    return TranslatableResult::yes();
    }

    /**
     * {@inheritdoc}
     */
    #[\ReturnTypeWillChange]
    public function abortTranslation(JobInterface $job) {
      // Assume that we can abort a translation job at any time.

    }

    /**
     * {@inheritdoc}
     */
    #[\ReturnTypeWillChange]
    public function requestTranslation(JobInterface $job) {

      $jobtranslator = $job->getTranslator();
      $jobtranslatorId = $jobtranslator->getPluginId();
      // Modify operations only for contentapi translator.
      if ($jobtranslatorId == 'contentapi') {
        $this->requestTranslationSource($job);
      }
    }

    /**
     * Request translation source.
     */
    private function requestTranslationSource(JobInterface $job) {
      // In the form by default only one option exist for task.
      // If in future more options are added, then this will be changed.
      if ($job->getSetting('capi-settings')["task"] == "trans") {
        // Submit job.
        \Drupal::service('tmgmt_contentapi.create_job')->submitJobToContentApi($job);
      }

    }

    /**
     * {@inheritdoc}
     */
    #[\ReturnTypeWillChange]
    public function hasCheckoutSettings(JobInterface $job) {
      return $job->getTranslator()->getSetting('allow_override');
    }

    /**
     * {@inheritdoc}
     */
    #[\ReturnTypeWillChange]
    public function defaultSettings() {
      return [
      'export_format' => 'contentapi_xlf',
      'allow_override' => TRUE,
      'scheme' => 'public',
      // Making this setting TRUE by default is more appropriate, however we
      // need to make it FALSE due to backwards compatibility.
      'xliff_processing' => FALSE,
      'xliff_cdata' => FALSE,
      'capi_username_ctt' => '',
      'one_export_file' => TRUE,
      'po_reference' => '',
      'transfer-settings' => FALSE
    ];
    }

    /**
     * {@inheritdoc}
     */
    #[\ReturnTypeWillChange]
    public function requestJobItemsTranslation(array $job_items) {
      // TODO: Implement requestJobItemsTranslation() method.

    }

}
