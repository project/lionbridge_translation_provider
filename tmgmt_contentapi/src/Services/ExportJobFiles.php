<?php

namespace Drupal\tmgmt_contentapi\Services;

use Drupal\Core\File\FileSystemInterface;
use Drupal\file\Entity\File;
use Drupal\File\FileRepositoryInterface;
use Drupal\File\FileUsage\FileUsageInterface;
use Drupal\node\Entity\Node;
use Drupal\tmgmt\JobInterface;
use Drupal\tmgmt_contentapi\Plugin\tmgmt_contentapi\Format\Xliff;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\tmgmt_contentapi\Services\JobHelper;
use ZipArchive;

/**
 * Service handle files related functionalities while job creation.
 *
 * @category Class
 * @package Drupal\tmgmt_contentapi\Services
 * @author Lionbridge team
 */
class ExportJobFiles {
  /**
   * The xliff object.
   *
   * @var \Drupal\tmgmt_contentapi\Plugin\tmgmt_contentapi\Format\Xliff
   */
  protected $exporter;

  /**
   * The filesystem service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The file repository service.
   *
   * @var \Drupal\File\FileRepositoryInterface
   */
  protected $fileRepository;

  /**
   * The file usage service.
   *
   * @var \Drupal\File\FileUsage\FileUsageInterface
   */
  protected $fileUsage;


  /**
   * Contains a reference to the currently being exported job.
   *
   * @var \Drupal\tmgmt\JobInterface
   */
  protected $job;

  /**
   * The clean job label.
   *
   * @var string
   */
  protected $joblabel;

  /**
   * Value for zip job path.
   */
  const ZIP_JOB_PATH = 'zip_job_';

  /**
   * Public file path for sent files.
   */
  const PUBLIC_SENT_FILE_PATH = '://tmgmt_contentapi/LioxSentFiles/';

  /**
  * Value for zip job path.
  */
  const ZIP_REF_PATH = 'zip_ref_';

  /**
   * Public file path for sent files.
   */
  const PUBLIC_REF_FILE_PATH = '://tmgmt_contentapi/LioxRefFiles/';

  /**
   * Zip extenstion.
   */
  const ZIP_EXTENSION = ".zip";

  /**
   * The zip file name with appropriate path.
   *
   * @var string
   */
  protected $zipName;

  /**
   * The zip file name with appropriate path.
   *
   * @var string
   */
  protected $dirNameAllFiles;

  /**
   * All files path.
   *
   * @var string
   */
  public $allFilesPath;

  /**
   * The zip path.
   *
   * @var string
   */
  public $zipPath;

  /**
   * File array to delete.
   *
   * @var array
   */
  public $fileArrayToDelte;

  /**
   * File array for exported files.
   *
   * @var array
   */
  public $fileArrayExportedFiles;

  /**
   * File array for transfer files.
   *
   * @var array
   */
  public $transferFiles;

  /**
   * Job helper service.
   *
   * @var \Drupal\tmgmt_contentapi\Services\JobHelper
   */
  protected $jobHelper;

  /**
   * Consructor.
   *
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   Object for file system service.
   * @param \Drupal\File\FileRepositoryInterface $fileRepository
   *   Object for file repository service.
   * @param \Drupal\File\FileUsage\FileUsageInterface $fileUsage
   *   Object for file usage service.
   * @param \Drupal\tmgmt_contentapi\Services\JobHelper $jobHelper
   *   Service for job helper.
   */
  public function __construct(
        FileSystemInterface $fileSystem,
        FileRepositoryInterface $fileRepository,
        FileUsageInterface $fileUsage,
        JobHelper $jobHelper,
    ) {
    $this->fileSystem = $fileSystem;
    $this->fileRepository = $fileRepository;
    $this->fileUsage = $fileUsage;
    $this->jobHelper = $jobHelper;
    // Set default values for variables.
    $this->exporter = new Xliff();
    $this->joblabel = '';
    $this->dirNameAllFiles = '';
    $this->zipName = '';
    $this->allFilesPath = '';
    $this->zipPath = '';
    // $this->fileArrayToDelte = [];
    // $this->fileArrayExportedFiles = [];
    // $this->transferFiles = [];
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
          $container->get('file.system'),
          $container->get('file.repository'),
          $container->get('file.usage'),
          $container->get('tmgmt_contentapi.job_helper')
      );
  }

  /**
   * Function to set joblabel.
   *
   * @param string $value
   *   Job label.
   */
  public function setJoblabel(string $value) {
    return $this->joblabel = $value;
  }

  /**
   * Function to set setZipName.
   *
   * @param string $value
   *   Zip name.
   */
  public function setZipName(string $value) {
    return $this->zipName = $value;
  }

  /**
   * Function to set dirNameAllFiles.
   *
   * @param string $value
   *   Directory name where all files are stored.
   */
  public function setDirNameAllFiles(string $value) {
    return $this->dirNameAllFiles = $value;
  }

  /**
   * Function to set allFilesPath.
   *
   * @param string $value
   *   Files path.
   */
  public function setAllFilesPath(string $value) {
    return $this->allFilesPath = $value;
  }

  /**
   * Function to set zipPath.
   *
   * @return string
   *   Zip path.
   */
  public function setZipPath(string $value) {
    return $this->zipPath = $value;
  }

  /**
   * Function to set fileArrayToDelte.
   *
   * @param array $value
   *   Files which needs to delete post job fail or success.
   */
  public function setFileArrayToDelte(array $value) {
    return $this->fileArrayToDelte = $value;
  }

  /**
   * Function to set fileArrayExportedFiles.
   *
   * @param array $value
   *   Of exported files.
   */
  public function setFileArrayExportedFiles(array $value) {
    return $this->fileArrayExportedFiles = $value;
  }

  /**
   * Function to set transferFiles.
   *
   * @param array $value
   *   Of transfer files.
   */
  public function setTransferFiles(array $value) {
    return $this->transferFiles = $value;
  }

  /**
   * Function to create folder where all exported files will get stored.
   *
   * @return bool
   *   Create a folder and return if its successfuly created or not.
   */
  public function createFolderToStoreExportedFiles() {
    return $this->fileSystem->prepareDirectory($this->allFilesPath, FileSystemInterface::CREATE_DIRECTORY);
  }

  /**
   * Function to export each item of job in same file.
   *
   * @return array
   *   Provide array of exported files.
   */
  public function singleExportFileForAllItems(JobInterface $job) {
    $this->fileArrayExportedFiles = [];
    $this->fileArrayToDelte = [];
    $name = $this->joblabel . "_" . $job->id() . "_all_" . $job->getRemoteSourceLanguage() . '_' . $job->getRemoteTargetLanguage() . '.xlf';
    $jobpath = $this->allFilesPath . "/" . $name;
    $file = $this->fileRepository->writeData($this->exporter->export($job), $jobpath, FileSystemInterface::EXISTS_REPLACE);
    $this->fileArrayToDelte[] = $file;
    $this->fileArrayExportedFiles['all'] = $file;
    return $this->fileArrayExportedFiles;
  }

  /**
   * Function to spearate Export file for each item of job.
   *
   * @param \Drupal\tmgmt\JobInterface $job
   *   Object for job.
   *
   * @return array
   *   Exported files.
   */
  public function separateExportFileForAllItems(JobInterface $job) {
    $this->fileArrayExportedFiles = [];
    $this->fileArrayToDelte = [];
    foreach ($job->getItems() as $item) {
      $labelname = $this->jobHelper->getStringNoSpeChars($item->label());
      $name = $labelname . "_" . $job->id() . "_" . $item->id() . "_" . $job->getRemoteSourceLanguage() . '_' . $job->getRemoteTargetLanguage() . '.xlf';
      $itempath = $this->allFilesPath . "/" . $name;
      $file = $this->fileRepository->writeData($this->exporter->exportItem($item), $itempath, FileSystemInterface::EXISTS_REPLACE);
      $this->fileArrayToDelte[] = $file;
      $this->fileArrayExportedFiles[$item->id()] = $file;
    }
    return $this->fileArrayExportedFiles;
  }

  /**
   * Function to zip exported file.
   *
   * @param \Drupal\tmgmt\JobInterface $job
   *   Object for job.
   */
  public function zipExportedFiles(JobInterface $job) {
    // Zip the exported files.
    if (empty($this->fileArrayExportedFiles)) {
      return;
    }
    $ziparchive = new \ZipArchive();
    $openresult = $ziparchive->open($this->fileSystem->realpath($this->zipPath), ZIPARCHIVE::CREATE | ZIPARCHIVE::OVERWRITE);
    $zipcloseresult = FALSE;
    $addedFiles = 0;
    if ($openresult) {
      foreach ($this->fileArrayExportedFiles as $tempfile) {
        if ($tempfile == NULL || $tempfile->getFileUri() == NULL || strlen($tempfile->getFileUri()) == 0) {
          continue;
        }
        if ($this->fileSystem->realpath($tempfile->getFileUri()) == NULL) {
          continue;
        }
        $ziparchive->addFile($this->fileSystem->realpath($tempfile->getFileUri()), $tempfile->getFilename());
        $addedFiles = $addedFiles + 1;
      }
      $zipcloseresult = FALSE;
      if ($addedFiles > 0) {
        $zipcloseresult = $ziparchive->close();
      }
      if ($zipcloseresult) {
        $zipfileobj = $this->jobHelper->createFileObject($this->zipPath);
        $this->fileUsage->add($zipfileobj, 'tmgmt_contentapi', 'tmgmt_job', $job->id());
      }
    }
  }

  /**
   * Check condition before moving ahead to job creation.
   *
   * @param \Drupal\tmgmt\JobInterface $job
   *   Object for job.
   */
  public function setConditions(JobInterface $job) {
    // Perform action only if capi setting task set to trans.
    if ($job->getSetting('capi-settings')["task"] != "trans") {
      return;
    }
    // Create folder to store exported files else throw exception.
    if (!$this->createFolderToStoreExportedFiles()) {
      throw new \Exception("Could not create directory for export: " . $this->allFilesPath);
    }
  }

  /**
   * Function to prepare files to transfer.
   *
   * @param \Drupal\tmgmt\JobInterface $job
   *   Object for job.
   */
  public function finalZipArrayOfFilesToTransfer(JobInterface $job) {

    // If exported files are transfered as zip, delete org. exports as already in the zip.
    if ($job->getTranslator()->getSetting('transfer-settings')) {
      // Add zip to transfer array.
      if (!file_exists($this->zipPath)) {
        return;
      }
      $this->transferFiles['all'] = $this->jobHelper->createFileObject($this->zipPath);
    }
    else {
      // All exported files added to transfer array.
      $this->transferFiles = $this->fileArrayExportedFiles;
    }
  }

  /**
   * Function to prepare reference files to transfer.
   *
   * @param \Drupal\tmgmt\JobInterface $job
   *   Object for job.
   *
   * @return array
   *   Reference file ready to transfer.
   */
  public function refFilesToTransfer(JobInterface $job) {
    $this->fileArrayExportedFiles = [];
    // Step 1: Initiate variables and set path.
    $this->joblabel = $this->setJoblabel($this->jobHelper->getJobLabelNoSpeChars($job));
    $this->dirNameAllFiles = $this->setDirNameAllFiles($this->joblabel . '_' . $job->id() . "_" . $job->getRemoteSourceLanguage() . "_" . $job->getRemoteTargetLanguage());
    $this->zipName = $this->setZipName(self::ZIP_REF_PATH . $this->dirNameAllFiles . self::ZIP_EXTENSION);
    $this->allFilesPath = $this->setAllFilesPath($job->getSetting('scheme') . self::PUBLIC_REF_FILE_PATH . $this->dirNameAllFiles);
    $this->zipPath = $this->setZipPath($this->allFilesPath . "/" . $this->zipName);
    // Step 2: Set condition as per the settings else return.
    $this->setConditions($job);
    // Step 3: Prepare files.
    foreach ($job->getItems() as $item) {
      // Node id.
      $nodeid = $item->getFields()["item_id"][0]->getValue()["value"];
      // To get target id.
      if (Node::load($nodeid) == NULL) {
        continue;
      }
      // Check if field_reference field exist for node.
      if (!Node::load($nodeid)->hasField("field_reference")) {
        continue;
      }
      // Check if field_reference field count is greater than 0.
      if (count(Node::load($nodeid)->getFields()["field_reference"]->getValue()) == 0) {
        continue;
      }
      // To get target id of field_reference field.
      $targetid = Node::load($nodeid)->getFields()["field_reference"]->getValue()[0]["target_id"];
      if ($targetid == NULL) {
        continue;
      }
      // Get the reference URL from targetid.
      $refFile = File::load($targetid);
      $this->fileArrayExportedFiles[$item->id()] = $refFile;
    }
    // Step 4: Add files to zip.
    // Check if files are there to zip.
    if (!empty($this->fileArrayExportedFiles)) {
      $this->zipExportedFiles($job);
    }

    // Step 5: Add zip to transfer array.
    $this->finalZipArrayOfFilesToTransfer($job);
    return $this->transferFiles;
  }

  /**
   * Function to prepare files to transfer.
   *
   * @param \Drupal\tmgmt\JobInterface $job
   *   Object for job.
   * @param bool $is_translation_memory
   *   Check if its translation memory.
   *
   * @return array
   *   Job files ready to transfer.
   */
  public function jobFilesToTransfer(JobInterface $job, bool $is_translation_memory = FALSE) {
    $this->transferFiles = [];
    // Step 1: Initiate variables and set path.
    $this->joblabel = $this->setJoblabel($this->jobHelper->getJobLabelNoSpeChars($job));
    // If is translation memory set then change the directory name.
    if ($is_translation_memory) {
      $this->dirNameAllFiles = $this->setDirNameAllFiles($this->joblabel . '_tmupdate_' . $job->id() . "_" . $job->getRemoteSourceLanguage() . "_" . $job->getRemoteTargetLanguage() . "_tm");
    }
    else {
      $this->dirNameAllFiles = $this->setDirNameAllFiles($this->joblabel . '_' . $job->id() . "_" . $job->getRemoteSourceLanguage() . "_" . $job->getRemoteTargetLanguage());
    }

    $this->zipName = $this->setZipName(self::ZIP_JOB_PATH . $this->dirNameAllFiles . self::ZIP_EXTENSION);
    $this->allFilesPath = $this->setAllFilesPath($job->getSetting('scheme') . self::PUBLIC_SENT_FILE_PATH . $this->dirNameAllFiles);
    $this->zipPath = $this->setZipPath($this->allFilesPath . "/" . $this->zipName);
    // Step 2: Set condition as per the settings else return.
    $this->setConditions($job);
    // Step 3: Prepare files.
    if ($job->getTranslator()->getSetting("one_export_file")) {
      $this->singleExportFileForAllItems($job);
    }
    else {
      $this->separateExportFileForAllItems($job);
    }
    // Step 4: Add files to zip.
    $this->zipExportedFiles($job);
    // Step 5: Add zip to transfer array.
    $this->finalZipArrayOfFilesToTransfer($job);
    return $this->transferFiles;
  }

  /**
   * Function to delete all files once job submitted.
   */
  public function deleteProcessedFiles() {
    foreach ($this->fileArrayToDelte as $tempfile) {
      $this->fileSystem->delete($tempfile->getFileUri());
    }
  }

}
