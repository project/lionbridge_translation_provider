<?php

namespace Drupal\tmgmt_contentapi\Plugin\views\field;

use Drupal\tmgmt_contentapi\Services\CapiDataProcessor;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\views\ViewExecutable;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A handler to provide a field that is completely custom by the administrator.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("job_lioxid_field")
 */
class JobLioxidField extends FieldPluginBase {

  /**
   * The current display.
   *
   * @var string
   *   The current display of the view.
   */
  protected $currentDisplay;

  /**
   * Capi data processor.
   *
   * @var \Drupal\tmgmt_contentapi\Services\CapiDataProcessor
   *   Capi data processor object
   */
  protected $capiDataProcessor;

  /**
   * Constructs a job_providerid_field object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\tmgmt_contentapi\Services\CapiDataProcessor $capi_data_processor
   *   Your Service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, CapiDataProcessor $capi_data_processor) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    // Inject your service.
    $this->capiDataProcessor = $capi_data_processor;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      // Add your service here to pass an instance to the constructor.
      $container->get('tmgmt_contentapi.capi_data_processor')
    );
  }

  /**
   * {@inheritdoc}
   */
  #[\ReturnTypeWillChange]
    public function init(ViewExecutable $view, DisplayPluginBase $display, array &$options = NULL) {
    parent::init($view, $display, $options);
    $this->currentDisplay = $view->current_display;
    }

    /**
     * {@inheritdoc}
     */
    #[\ReturnTypeWillChange]
    public function usesGroupBy() {
      return FALSE;
    }

    /**
     * {@inheritdoc}
     */
    #[\ReturnTypeWillChange]
    public function query() {
      // Do nothing -- to override the parent query.
    }

    /**
     * {@inheritdoc}
     */
    protected function defineOptions() {
      $options = parent::defineOptions();
      // First check whether the field should be hidden if the value(hide_alter_empty = TRUE) /the rewrite is empty (hide_alter_empty = FALSE).
      $options['hide_alter_empty'] = ['default' => FALSE];
      return $options;
    }

    /**
     * {@inheritdoc}
     */
    #[\ReturnTypeWillChange]
    public function render(ResultRow $values) {
      // Get capi jobid for the current job.
      $job_local_details = $this->capiDataProcessor->getGlobalColumnInfoForJobOverview($values->tjid);
      if (empty($job_local_details['jobid'])) {
        return 'N/A';
      }
      return $job_local_details['jobid'];
    }

}
