<?php

namespace Drupal\tmgmt_contentapi\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Format plugin annotation object.
 *
 * @Annotation
 *
 * @see \Drupal\tmgmt_contentapi\Format\FormatManager
 */
class FormatPlugin extends Plugin {

  /**
   * The plugin id.
   *
   * @var string
   *   The plugin ID.
   */
  public $id;

  /**
   * The label.
   *
   * @var \Drupal\Core\Annotation\Translation
   *   The human-readable name of the format.
   *
   * @ingroup plugin_translatable
   */
  public $label;

}
